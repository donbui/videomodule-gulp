// TODO: function for next/prev in array (generator function? Vue computed?)
// TODO: "Uncaught (in promise) DOMException: The play() request was interrupted by a new load request." https://developers.google.com/web/updates/2017/06/play-request-was-interrupted

var isMobile, isEdge, app, nullObject, container, bg, videoInfo, bgContext, fullRotation, rotationStep, faceWidth, faceHeight, screenWidth, faceZOriginX, faceZOriginY, myCubeDraggable, swipeDirection, cubeRotatedX, cubeRotatedY, frontFace, rightFace, leftFace, topFace, bottomFace, xFaces = [], yFaces = [], videoId, videoPlayer, videoAspectRatio = 9/16;
var navModes = ['shows', 'episodes', 'playlists'], targetShows = [], targetEpisodes = [], targetVideos = [], targetPlaylists = [], currentNavMode = navModes[0], currentShow, nextShow, prevShow, currentEpisode, nextEpisode, prevEpisode, currentPlaylist, nextPlaylist, prevPlaylist, currentVideo, nextVideo, prevVideo;
var mPlayer, ccDisplay, ccButton, progressBar, controlBar, playControl, parentTitle, mainTitle, subTitle, transitionReady = false;

function initUI() {
	isMobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);
	isEdge = window.navigator.userAgent.indexOf("Edge") > -1;

	processQueryString();
	getScreenDimensions();
	fullRotation = 360;
	rotationStep = 90;
	faceZOriginX = getZOriginX();
	faceZOriginY = getZOriginY();

	app = document.querySelector('#app');
	TweenMax.set(app, {width:screenWidth, height:faceHeight, y:0, backgroundColor:'black', overflow:'hidden'});

	nullObject = document.createElement('div');
	nullObject.className = 'null-object';
	app.appendChild(nullObject);
	TweenMax.set(nullObject, {position:'absolute', x:0})

	container = document.querySelector('.container');
	TweenMax.set(container, {width:screenWidth, height:faceHeight});

	bg = document.getElementById('backgroundVideo');
	bgContext = bg.getContext('2d');
	bgContext.filter = 'blur(10px) grayscale(30%)';
	TweenMax.set(bg, {z:-faceWidth});

	videoInfo = document.getElementById('videoInfo');
	TweenMax.set(videoInfo, {
		width: (screenWidth-faceWidth)/2 < 360 ? screenWidth : (screenWidth-faceWidth)/2,
		height: faceHeight,
		alpha: 0,
		z: (faceHeight)/2
	})

	parentTitle = document.getElementById('parentTitle');
	mainTitle = document.getElementById('mainTitle');
	subTitle = document.getElementById('subTitle');

	dPadAddListeners();
	document.addEventListener('keyup', keyboardControls, false);

	if(window.ga && ga.loaded) ga('create', 'UA-104644255-4', 'auto');
}

window.addEventListener('resize', function() {
	getScreenDimensions();
	TweenMax.set(videoInfo, {width:Math.max((screenWidth-faceWidth)/2, faceWidth), height:faceHeight})
	if (currentNavMode === 'intro') {
		TweenMax.set(frontFace, {width:faceHeight*16/9, height:faceHeight})
	} else {
		TweenMax.set([app, container, bg], {width:screenWidth, height:faceHeight});
		TweenMax.set([videoPlayer, frontFace], {width:faceWidth, height:faceHeight});
		mPlayer.width(faceWidth)
		mPlayer.height(faceHeight)
		TweenMax.set(videoInfo, {
			width: (screenWidth-faceWidth)/2 < 360 ? screenWidth : (screenWidth-faceWidth)/2,
			height: faceHeight,
			z: 10
		})
		mPlayer.width(faceWidth)
		mPlayer.height(faceHeight)
		faceZOriginX = getZOriginX();
		faceZOriginY = getZOriginY();
		refreshFaces();
	}
}, false)

function setCubeDraggable() {
	myCubeDraggable = null;
	myCubeDraggable = Draggable.create(nullObject, {
		type:'x, y',
		lockAxis:true,
		trigger:container,
		dragResistance:0.7,
		throwResistance:100,
		throwProps:true,
		onDrag:onDrag,
		onDragEnd:onDragEnd,
		onClick:onClick,
		onRelease:onRelease,
		zIndexBoost:false,
		onThrowUpdate:onDrag,
		onThrowComplete:onThrowComplete,
		ease:Back.easeOut.config(0.3),
		overshootTolerance: 0,
		allowEventDefault: true,
		bounds: {
			minX: -rotationStep,
			minY: -rotationStep,
			maxX: rotationStep,
			maxY: rotationStep
		},
		snap: {
			x: function(endValue) {
				return Math.round(endValue/rotationStep) * rotationStep;
			},
			y: function(endValue) {
				return Math.round(endValue/rotationStep) * rotationStep;
			}
		}
	})
}

function onClick(event) {
	if (event.target === ccButton || event.target === progressBar) { return false };
	if (currentNavMode === 'shows') {
		currentNavMode = 'episodes';
		refreshFaces();
		mPlayer.muted(false);
		mPlayer.play();
		console.log("Nav Mode: " + currentNavMode.toUpperCase());
	} else if (currentNavMode === 'playlists') {
		currentNavMode = 'videos';
		refreshFaces();
		mPlayer.muted(false);
		mPlayer.play();
		console.log("Nav Mode: " + currentNavMode.toUpperCase());
	} else {
		if (isMobile && mPlayer.paused()) {
			mPlayer.play();
		} else if (isMobile && !mPlayer.paused()) {
			mPlayer.pause();
		}
	}
}

function onRelease() {
	this.applyBounds({minX: -rotationStep, minY: -rotationStep, maxX: rotationStep, maxY: rotationStep})
	zoomIn();
}

function onDragEnd() {
	this.applyBounds({minX: -rotationStep, minY: -rotationStep, maxX: rotationStep, maxY: rotationStep})
	myCubeDraggable[0].disable();
	zoomIn();
}

function onDrag() {
	zoomOut();
	// TweenMax.set(frontFace, {alpha:0})

	transitionReady = true;
	if (Math.abs(nullObject._gsTransform.x) < 6 && Math.abs(nullObject._gsTransform.y) < 6) {
		hideInfo();
	}

	if (nullObject._gsTransform.x < -rotationStep/2) {
		this.applyBounds({minX: -rotationStep, minY: -rotationStep, maxX: nullObject._gsTransform.x, maxY: rotationStep});
	} else if (nullObject._gsTransform.x > rotationStep/2) {
		this.applyBounds({minX: nullObject._gsTransform.x, minY: -rotationStep, maxX: rotationStep, maxY: rotationStep});
	} else if (nullObject._gsTransform.y < -rotationStep/2) {
		this.applyBounds({minX: -rotationStep, minY: -rotationStep, maxX: rotationStep, maxY: nullObject._gsTransform.y});
	} else if (nullObject._gsTransform.y > rotationStep/2) {
		this.applyBounds({minX: -rotationStep, minY: nullObject._gsTransform.y, maxX: rotationStep, maxY: rotationStep});
	}

	if (!mPlayer.paused()) {mPlayer.pause()};

	swipeDirection = this.getDirection('start')

	if (swipeDirection === 'up' || swipeDirection === 'down') {
		TweenMax.set(xFaces, {alpha:1})
		TweenMax.set(yFaces, {alpha:0})
		if (nullObject._gsTransform.y > 0) {
			TweenMax.set(topFace, {zIndex:1});
			TweenMax.set(bottomFace, {zIndex:-1});
		} else {
			TweenMax.set(topFace, {zIndex:-1});
			TweenMax.set(bottomFace, {zIndex:1});
		}
		for (var i = 0; i < xFaces.length; i++) {
			TweenMax.set(xFaces[i], {
				rotationX:(xFaces[i].initRotationX - (nullObject._gsTransform.y % fullRotation)),
				ease:Power4.easeOut,
				force3D:true
			})
		}
		TweenMax.set([frontFace, videoPlayer], {
			transformOrigin:'50% 50% ' + -faceZOriginX + 'px',
			rotationX:(frontFace.initRotationX - (nullObject._gsTransform.y % fullRotation)),
			ease:Power4.easeOut,
			force3D:true
		})
	} else if (swipeDirection === 'left' || swipeDirection === 'right') {
		TweenMax.set(xFaces, {alpha:0})
		TweenMax.set(yFaces, {alpha:1})
		if (nullObject._gsTransform.x > 0) {
			TweenMax.set(rightFace, {zIndex:-1});
			TweenMax.set(leftFace, {zIndex:1});
		} else {
			TweenMax.set(rightFace, {zIndex:1});
			TweenMax.set(leftFace, {zIndex:-1});
		}
		for (var j = 0; j < yFaces.length; j++) {
			TweenMax.set(yFaces[j], {
				rotationY:(yFaces[j].initRotationY + (nullObject._gsTransform.x % fullRotation)),
				ease:Power4.easeOut,
				force3D:true
			})
		}
		TweenMax.set([frontFace, videoPlayer], {
			transformOrigin:'50% 50% ' + -faceZOriginY + 'px',
			rotationY:(frontFace.initRotationY + (nullObject._gsTransform.x % fullRotation)),
			ease:Power4.easeOut,
			force3D:true
		})
	}
}

function createFrontFace() {
	var face;
	face = document.createElement('div');
	face.className = 'face';
	TweenMax.set(face, {position:'absolute', width:faceWidth, height:faceHeight, zIndex:0, transformPerspective:1000, backfaceVisibility:'hidden', transformOrigin:'50% 50% ' + -faceZOriginX + 'px', boxSizing:'border-box', rotation:0.001, force3D:true});
	face.initRotationX = 0;
	face.initRotationY = 0;
	frontFace = face;
	container.appendChild(face);

	var index;
	var playerHTML;
	videoId = 'videoPlayer';

	if (currentNavMode === 'shows') {
		frontFace.appendChild(currentShow.firstEpisode.cover);
		playerHTML = `<video width="${faceWidth}" height="${faceHeight}" data-account="${account_id}" data-player="H1maWx6EX" data-playlist-id="${episodesPlaylistId}" data-embed="default" data-application-id id="${videoId}" class="video-js" controls playsinline ></video>`
		for (var i = 0; i < targetEpisodes.length; i++) {
			if (targetEpisodes[i].show === currentShow.title) {
				index = i;
				break;
			}
		}
	} else if (currentNavMode === 'episodes') {
		frontFace.appendChild(currentEpisode.cover);
		playerHTML = `<video width="${faceWidth}" height="${faceHeight}" data-account="${account_id}" data-player="H1maWx6EX" data-playlist-id="${episodesPlaylistId}" data-embed="default" data-application-id id="${videoId}" class="video-js" controls playsinline ></video>`
		index = targetEpisodes.indexOf(currentEpisode);
	} else if (currentNavMode === 'playlists') {
		frontFace.appendChild(currentPlaylist.firstVideo.cover);
		playerHTML = `<video width="${faceWidth}" height="${faceHeight}" data-account="${account_id}" data-player="H1maWx6EX" data-playlist-id="${videosPlaylistId}" data-embed="default" data-application-id id="${videoId}" class="video-js" controls playsinline ></video>`
		for (var i = 0; i < targetVideos.length; i++) {
			if (targetVideos[i].playlist === currentPlaylist.title) {
				index = i;
				break;
			}
		}
	} else if (currentNavMode === 'intro') {
		playerHTML = `<video width="${faceWidth}" height="${faceHeight}" data-account="${account_id}" data-player="H1maWx6EX" data-playlist-id="${introPlaylist}" data-embed="default" data-application-id id="${videoId}" class="video-js" controls playsinline ></video>`
	}

	container.insertAdjacentHTML('beforeend', playerHTML);
	document.querySelector('video').setAttribute("controls","controls");
	bc(document.getElementById(videoId));

	videojs(videoId).ready(function() {
		mPlayer = this;
		mPlayer.width(faceWidth)
		mPlayer.height(faceHeight)
		ccDisplay = document.getElementsByClassName('vjs-text-track-display')[0];
		TweenMax.set(ccDisplay, {z: (faceHeight)/2, zIndex:9999})
		playControl = document.getElementsByClassName('vjs-play-control')[0];
		hidePreloader();
		playControl.tabIndex = 0;
		playControl.setAttribute('aria-label', 'play');
		videoPlayer = document.getElementById('videoPlayer_html5_api');
		TweenMax.set(videoPlayer, {position:'absolute', width:faceWidth, height:faceHeight, transformPerspective:1000, backfaceVisibility:'hidden', transformOrigin:'50% 50% ' + -faceZOriginX + 'px'});
		if (currentNavMode === 'shows') {
			mPlayer.currentTime(currentEpisode.loop_start);
		} else if (currentNavMode === 'playlists') {
			mPlayer.currentTime(currentVideo.loop_start);
		}
		showInfo();

		controlBar = document.getElementsByClassName('vjs-control-bar')[0];
		progressBar = document.getElementsByClassName('vjs-progress-control')[0];
		TweenMax.set(controlBar, {z: (faceHeight)/2})
	});
	videoListeners();
}

function destroyAdjFaces() {
	var faces = document.getElementsByClassName('face');
	for (var i = 0; i < faces.length; i++) {
		if ((faces[i]._gsTransform.rotationX === 0) && (faces[i]._gsTransform.rotationY === 0)) {
			frontFace = faces[i];
			break;
		}
	}
	while (faces.length > 1) {
		if (faces[0] !== frontFace) {
			faces[0].innerHTML = '';
			container.removeChild(faces[0]);
		}
		if (faces[1] && faces[1] !== frontFace) {
			faces[1].innerHTML = '';
			container.removeChild(faces[1]);
		}
	}

	TweenMax.set(frontFace, {rotationX:0, rotationY:0, zIndex:0});
	frontFace.initRotationX = 0;
	frontFace.initRotationY = 0;
	xFaces = [];
	yFaces = [];
	TweenMax.set([xFaces, yFaces], {rotationX:0, rotationY:0})
	TweenMax.set(nullObject, {x:0, y:0})
}

function createAdjFaces() {
	rightFace = document.createElement('div');
	rightFace.className = 'face';
	TweenMax.set(rightFace, {position:'absolute', width:faceWidth, height:faceHeight, zIndex:-1, transformPerspective:1000, backfaceVisibility:'hidden', transformOrigin:'50% 50% ' + -faceZOriginY + 'px', boxSizing:'border-box', rotationY: rotationStep, force3D:true});
	rightFace.initRotationX = 0;
	rightFace.initRotationY = rotationStep;
	yFaces.push(rightFace);
	container.appendChild(rightFace);

	leftFace = document.createElement('div');
	leftFace.className = 'face';
	TweenMax.set(leftFace, {position:'absolute', width:faceWidth, height:faceHeight, zIndex:-1, transformPerspective:1000, backfaceVisibility:'hidden', transformOrigin:'50% 50% ' + -faceZOriginY + 'px', boxSizing:'border-box', rotationY: -rotationStep, force3D:true});
	leftFace.initRotationX = 0;
	leftFace.initRotationY = -rotationStep;
	yFaces.push(leftFace);
	container.appendChild(leftFace);

	topFace = document.createElement('div');
	topFace.className = 'face';
	TweenMax.set(topFace, {position:'absolute', width:faceWidth, height:faceHeight, zIndex:-1, transformPerspective:1000, backfaceVisibility:'hidden', transformOrigin:'50% 50% ' + -faceZOriginX + 'px', boxSizing: 'border-box', rotationX: rotationStep, force3D:true});
	topFace.initRotationX = rotationStep;
	topFace.initRotationY = 0;
	xFaces.push(topFace);
	container.appendChild(topFace);

	bottomFace = document.createElement('div');
	bottomFace.className = 'face';
	TweenMax.set(bottomFace, {position:'absolute', width:faceWidth, height:faceHeight, zIndex:-1, transformPerspective:1000, backfaceVisibility:'hidden', transformOrigin:'50% 50% ' + -faceZOriginX + 'px', boxSizing: 'border-box', rotationX: -rotationStep, force3D:true});
	bottomFace.initRotationX = -rotationStep;
	bottomFace.initRotationY = 0;
	xFaces.push(bottomFace);
	container.appendChild(bottomFace);
}

function updateFaces() {
	updateBackgroundVideo();
	TweenMax.set('.desktopControls', {alpha:1})
	if (currentNavMode === 'shows') {
		topFace.appendChild(currentVideo.cover.cloneNode(true));
		bottomFace.appendChild(currentEpisode.cover.cloneNode(true));
		rightFace.appendChild(nextShow.cover.cloneNode(true));
		leftFace.appendChild(prevShow.cover.cloneNode(true));
		TweenMax.set('.vjs-control-bar', {alpha:0})
	} else if (currentNavMode === 'episodes') {
		topFace.appendChild(currentEpisode.cover.cloneNode(true));
		bottomFace.appendChild(currentVideo.cover.cloneNode(true));
		rightFace.appendChild(nextEpisode.cover.cloneNode(true));
		leftFace.appendChild(prevEpisode.cover.cloneNode(true));
		mPlayer.muted(false);
		TweenMax.set('.vjs-control-bar', {alpha:1})
	} else if (currentNavMode === 'playlists') {
		topFace.appendChild(currentEpisode.cover.cloneNode(true));
		bottomFace.appendChild(currentEpisode.cover.cloneNode(true));
		rightFace.appendChild(nextPlaylist.firstVideo.cover.cloneNode(true));
		leftFace.appendChild(prevPlaylist.firstVideo.cover.cloneNode(true));
		TweenMax.set('.vjs-control-bar', {alpha:0})
	} else if (currentNavMode === 'videos') {
		topFace.appendChild(currentVideo.cover.cloneNode(true));
		bottomFace.appendChild(currentVideo.cover.cloneNode(true));
		rightFace.appendChild(nextVideo.cover.cloneNode(true));
		leftFace.appendChild(prevVideo.cover.cloneNode(true));
		mPlayer.muted(false);
		TweenMax.set('.vjs-control-bar', {alpha:1})
	}
	mPlayer.pause();
	if (currentNavMode === 'shows' || currentNavMode === 'episodes') {
		// bgContext.drawImage(currentEpisode.cover, 0, 0, bg.width, bg.height);
		mPlayer.catalog.load(episodesPlaylist);
		mPlayer.playlist.currentItem(targetEpisodes.indexOf(currentEpisode));
	} else if (currentNavMode === 'playlists' || currentNavMode === 'videos') {
		// bgContext.drawImage(currentEpisode.cover, 0, 0, bg.width, bg.height);
		mPlayer.catalog.load(videosPlaylist);
		mPlayer.playlist.currentItem(targetVideos.indexOf(currentVideo));
	}
	if ((currentNavMode === 'shows' || currentNavMode === 'playlists') && mPlayer.muted() === false) {
		mPlayer.muted(true);
	}

	TweenMax.set(videoPlayer, {rotationX:0, rotationY:0, alpha:0, zIndex:1, z:10})

	// // AUTOPLAY on player mode only
	// if (currentNavMode === 'episodes' || currentNavMode === 'videos') {
	// 	mPlayer.play();
	// }
	// TweenMax.to(videoPlayer, 0.3, {alpha:1, delay:0.3})
}

function refreshFaces() {
	destroyAdjFaces();
	createAdjFaces();
	updateFaces();
}

function onThrowComplete() {
	myCubeDraggable[0].applyBounds({minX: -rotationStep, minY: -rotationStep, maxX: rotationStep, maxY: rotationStep})
	zoomIn();
	container.style.cursor = 'wait';
	transitionReady = false;

	cubeRotatedX = (nullObject._gsTransform.x !== 0) ? true : false;
	cubeRotatedY = (nullObject._gsTransform.y !== 0) ? true : false;

	if (cubeRotatedY && (swipeDirection === 'up' || swipeDirection === 'down')) {
		if (currentNavMode === 'videos') {
			currentNavMode = 'playlists';
		} else {
			var i = navModes.indexOf(currentNavMode);
			if (swipeDirection === 'up') {
				i = ((i + 1) + navModes.length) % navModes.length;
			} else if (swipeDirection === 'down') {
				i = ((i - 1) + navModes.length) % navModes.length;
			}
			currentNavMode = navModes[i];
		}

		console.log("Nav Mode: " + currentNavMode.toUpperCase());
	} else if (currentNavMode === 'shows' && cubeRotatedX && (swipeDirection === 'left' || swipeDirection === 'right')) { // iterating through Shows

		var i = targetShows.indexOf(currentShow);
		if (swipeDirection === 'left') {
			i = ((i + 1) + targetShows.length) % targetShows.length;
		} else if (swipeDirection === 'right') {
			i = ((i - 1) + targetShows.length) % targetShows.length;
		}
		currentShow = targetShows[i];
		nextShow = targetShows[((i + 1) + targetShows.length) % targetShows.length];
		prevShow = targetShows[((i - 1) + targetShows.length) % targetShows.length];

		getCurrentEpisodeFromShow();

		console.log("Show: " + currentShow.title);
	} else if (currentNavMode === 'episodes' && cubeRotatedX && (swipeDirection === 'left' || swipeDirection === 'right')) { // iterating through Episodes
		var i = targetEpisodes.indexOf(currentEpisode);
		if (swipeDirection === 'left') {
			i = ((i + 1) + targetEpisodes.length) % targetEpisodes.length;
		} else if (swipeDirection === 'right') {
			i = ((i - 1) + targetEpisodes.length) % targetEpisodes.length;
		}
		currentEpisode = targetEpisodes[i];
		nextEpisode = targetEpisodes[((i + 1) + targetEpisodes.length) % targetEpisodes.length];
		prevEpisode = targetEpisodes[((i - 1) + targetEpisodes.length) % targetEpisodes.length];

		if (currentEpisode.show !== currentShow.title) {
			for (var j = 0; j < targetShows.length; j++) {
				if (currentEpisode.show === targetShows[j].title) {
					currentShow = targetShows[j];
					nextShow = targetShows[((i + 1) + targetShows.length) % targetShows.length];
					prevShow = targetShows[((i - 1) + targetShows.length) % targetShows.length];
				}
			}
		}

		console.log("Episode: " + currentEpisode.title);
	} else if (currentNavMode === 'playlists' && cubeRotatedX && (swipeDirection === 'left' || swipeDirection === 'right')) { // iterating through Playlists
		var i = targetPlaylists.indexOf(currentPlaylist);
		if (swipeDirection === 'left') {
			i = ((i + 1) + targetPlaylists.length) % targetPlaylists.length;
		} else if (swipeDirection === 'right') {
			i = ((i - 1) + targetPlaylists.length) % targetPlaylists.length;
		}
		currentPlaylist = targetPlaylists[i];
		nextPlaylist = targetPlaylists[((i + 1) + targetPlaylists.length) % targetPlaylists.length];
		prevPlaylist = targetPlaylists[((i - 1) + targetPlaylists.length) % targetPlaylists.length];

		getCurrentVideoFromPlaylist();

		console.log("Playlist: " + currentPlaylist.title);
	} else if (currentNavMode === 'videos' && cubeRotatedX && (swipeDirection === 'left' || swipeDirection === 'right')) { // iterating through Playlists
		var i = targetVideos.indexOf(currentVideo);
		if (swipeDirection === 'left') {
			i = ((i + 1) + targetVideos.length) % targetVideos.length;
		} else if (swipeDirection === 'right') {
			i = ((i - 1) + targetVideos.length) % targetVideos.length;
		}
		currentVideo = targetVideos[i];
		nextVideo = targetVideos[((i + 1) + targetVideos.length) % targetVideos.length];
		prevVideo = targetVideos[((i - 1) + targetVideos.length) % targetVideos.length];

		if (currentVideo.playlist !== currentPlaylist.title) {
			for (var j = 0; j < targetPlaylists.length; j++) {
				if (currentVideo.playlist === targetPlaylists[j].title) {
					currentPlaylist = targetPlaylists[j];
					nextPlaylist = targetPlaylists[((i + 1) + targetPlaylists.length) % targetPlaylists.length];
					prevPlaylist = targetPlaylists[((i - 1) + targetPlaylists.length) % targetPlaylists.length];
				}
			}
		}

		console.log("Playlist Video: " + currentVideo.title);
	}

	if (cubeRotatedX || cubeRotatedY) {
		refreshFaces();
		if (currentNavMode === 'shows' && window.ga && ga.loaded) {
				ga('send', 'pageview', `/video/show/${currentShow.title}`);
		} else if (currentNavMode === 'episodes' && window.ga && ga.loaded) {
				ga('send', 'pageview', `/video/show/${currentShow.title}/episode/${currentEpisode.title}`);
		} else if (currentNavMode === 'playlists' && window.ga && ga.loaded) {
				ga('send', 'pageview', `/video/playlist/${currentPlaylist.title}`);
		} else if (currentNavMode === 'videos' && window.ga && ga.loaded) {
				ga('send', 'pageview', `/video/playlist/${currentPlaylist.title}/video/${currentVideo.title}`);
		}
	}

	// TweenMax.delayedCall(0.75, function() {
		myCubeDraggable[0].enable();
		showInfo();
		container.style.cursor = 'move'
		document.addEventListener('keyup', keyboardControls, false)
		dPadAddListeners();
	// })
}

function rotateCube(e) {
	var direction = (e.target && e.target.dataset && e.target.dataset.direction) ? e.target.dataset.direction : e;
	if (direction !== 'up' && direction !== 'down' && direction !== 'left' && direction !== 'right') {
		return false;
	}

	transitionReady = true;
	hideInfo();
	dPadRemoveListeners();
	mPlayer.pause();
	zoomOut();

	if (direction === 'up' || direction === 'down') {
		TweenMax.set(xFaces, {alpha:1})
		TweenMax.set(yFaces, {alpha:0})
		if (direction === 'down') {
			swipeDirection = 'up';
			// TweenMax.set(videoPlayer, {css:{boxShadow:'0px 4px 0px #fde147'}});
			// TweenMax.set(bottomFace, {css:{boxShadow:'0px -4px 0px #fde147'}});
			document.querySelector('.navButton[data-direction="down"]').classList.remove('hover');
			TweenMax.set(topFace, {zIndex:-1});
			TweenMax.set(bottomFace, {zIndex:0});
			TweenMax.set(nullObject, {y:-90});
			TweenMax.to([videoPlayer, frontFace, xFaces], 0.5, {
				transformOrigin:'50% 50% ' + -faceZOriginX + 'px',
				rotationX:`+=90`,
				ease:Power4.easeOut,
				force3D:true,
				onComplete:function() {
					onThrowComplete();
					showInfo();
				}
			})
		} else {
			swipeDirection = 'down';
			// TweenMax.set(videoPlayer, {css:{boxShadow:'0px -4px 0px #fde147'}});
			// TweenMax.set(topFace, {css:{boxShadow:'0px 4px 0px #fde147'}});
			document.querySelector('.navButton[data-direction="up"]').classList.remove('hover');
			TweenMax.set(topFace, {zIndex:0});
			TweenMax.set(bottomFace, {zIndex:-1});
			TweenMax.set(nullObject, {y:90})
			TweenMax.to([videoPlayer, frontFace, xFaces], 0.5, {
				transformOrigin:'50% 50% ' + -faceZOriginX + 'px',
				rotationX:`-=90`,
				ease:Power4.easeOut,
				force3D:true,
				onComplete:function() {
					onThrowComplete();
					showInfo();
				}
			})
		}
	} else if (direction === 'left' || direction === 'right') {
		TweenMax.set(xFaces, {alpha:0})
		TweenMax.set(yFaces, {alpha:1})
		if (direction === 'right') {
			swipeDirection = 'left';
			// TweenMax.set(videoPlayer, {css:{boxShadow:'4px 0px 0px #fde147'}});
			// TweenMax.set(rightFace, {css:{boxShadow:'-4px 0px 0px #fde147'}});
			document.querySelector('.navButton[data-direction="right"]').classList.remove('hover');
			TweenMax.set(rightFace, {zIndex:0});
			TweenMax.set(leftFace, {zIndex:-1});
			TweenMax.set(nullObject, {x:-90})
			TweenMax.to([videoPlayer, frontFace, yFaces], 0.5, {
				transformOrigin:'50% 50% ' + -faceZOriginY + 'px',
				rotationY:`-=90`,
				ease:Power4.easeOut,
				force3D:true,
				onComplete:function() {
					onThrowComplete();
					showInfo();
				}
			})
		} else {
			swipeDirection = 'right';
			// TweenMax.set(videoPlayer, {css:{boxShadow:'-4px 0px 0px #fde147'}});
			// TweenMax.set(leftFace, {css:{boxShadow:'4px 0px 0px #fde147'}});
			document.querySelector('.navButton[data-direction="left"]').classList.remove('hover');
			TweenMax.set(rightFace, {zIndex:-1});
			TweenMax.set(leftFace, {zIndex:0});
			TweenMax.set(nullObject, {x:90})
			TweenMax.to([videoPlayer, frontFace, yFaces], 0.5, {
				transformOrigin:'50% 50% ' + -faceZOriginY + 'px',
				rotationY:`+=90`,
				ease:Power4.easeOut,
				force3D:true,
				onComplete:function() {
					onThrowComplete();
					showInfo();
				}
			})
		}
	}
}

function getCurrentEpisodeFromShow() {
	currentEpisode = currentShow.firstEpisode;
	var j = targetEpisodes.indexOf(currentEpisode);
	nextEpisode = targetEpisodes[((j + 1) + targetEpisodes.length) % targetEpisodes.length];
	prevEpisode = targetEpisodes[((j - 1) + targetEpisodes.length) % targetEpisodes.length];
}
function getCurrentVideoFromPlaylist() {
	currentVideo = currentPlaylist.firstVideo;
	var j = targetVideos.indexOf(currentVideo);
	nextVideo = targetVideos[((j + 1) + targetVideos.length) % targetVideos.length];
	prevVideo = targetVideos[((j - 1) + targetVideos.length) % targetVideos.length];
}

function processQueryString() {
	var urlParams = new URLSearchParams(window.location.search);
	if (navModes.indexOf(urlParams.get('mode')) > -1) {
		currentNavMode = urlParams.get('mode');
	} else if (urlParams.get('mode') === 'random') {
		currentNavMode = 'episodes'
		console.log("Nav Mode: " + currentNavMode.toUpperCase());
		var i = Math.floor(Math.random()*(targetEpisodes.length));
		currentEpisode = targetEpisodes[i];
		nextEpisode = targetEpisodes[((i + 1) + targetEpisodes.length) % targetEpisodes.length];
		prevEpisode = targetEpisodes[((i - 1) + targetEpisodes.length) % targetEpisodes.length];
		for (var j = 0; j < targetShows.length; j++) {
			if (currentEpisode.show === targetShows[j].title) {
				currentShow = targetShows[j];
				nextShow = targetShows[((i + 1) + targetShows.length) % targetShows.length];
				prevShow = targetShows[((i - 1) + targetShows.length) % targetShows.length];
			}
		}
	}	else if (urlParams.get('episode') && urlParams.get('show')) {
		currentNavMode = 'episodes';
		for (var i = 0; i < targetEpisodes.length; i++) {
			if (underscoreEncode(targetEpisodes[i].title) === underscoreEncode(urlParams.get('episode')) && underscoreEncode(targetEpisodes[i].show) === underscoreEncode(urlParams.get('show'))) {
				currentEpisode = targetEpisodes[i];
				nextEpisode = targetEpisodes[((i + 1) + targetEpisodes.length) % targetEpisodes.length];
				prevEpisode = targetEpisodes[((i - 1) + targetEpisodes.length) % targetEpisodes.length];
				break;
			}
		}
		for (var i = 0; i < targetShows.length; i++) {
			if (targetShows[i].title === currentEpisode.show) {
				currentShow = targetShows[i];
				nextShow = targetShows[((i + 1) + targetShows.length) % targetShows.length];
				prevShow = targetShows[((i - 1) + targetShows.length) % targetShows.length];
				break;
			}
		}
	} else if (urlParams.get('show')) {
		currentNavMode = 'shows';
		for (var i = 0; i < targetShows.length; i++) {
			if (underscoreEncode(targetShows[i].title) === underscoreEncode(urlParams.get('show'))) {
				currentShow = targetShows[i];
				nextShow = targetShows[((i + 1) + targetShows.length) % targetShows.length];
				prevShow = targetShows[((i - 1) + targetShows.length) % targetShows.length];
				break;
			}
		}
		getCurrentEpisodeFromShow();
	} else if (urlParams.get('playlist')) {
		currentNavMode = 'playlists';
		for (var i = 0; i < targetPlaylists.length; i++) {
			if (underscoreEncode(targetPlaylists[i].title) === underscoreEncode(urlParams.get('show'))) {
				currentPlaylist = targetPlaylists[i];
				nextPlaylist = targetPlaylists[((i + 1) + targetPlaylists.length) % targetPlaylists.length];
				prevPlaylist = targetPlaylists[((i - 1) + targetPlaylists.length) % targetPlaylists.length];
				break;
			}
		}
		getCurrentVideoFromPlaylist();
	}
}

function dPadAddListeners() {
	document.querySelector('.desktopControls').addEventListener('click', rotateCube, false);
	document.querySelector('.desktopControls').addEventListener('mouseover', desktopControlsMouseOver, false);
	document.querySelector('.desktopControls').addEventListener('mouseout', desktopControlsMouseOut, false);

	document.querySelector('.desktopControls').addEventListener('keypress', function (e) {
		if (e.key === 'Enter') {
			rotateCube(e.srcElement.attributes['data-direction'].value);
		}
	});

}
function dPadRemoveListeners() {
	document.querySelector('.desktopControls').removeEventListener('click', rotateCube, false);
	document.querySelector('.desktopControls').removeEventListener('mouseover', desktopControlsMouseOver, false);
	document.querySelector('.desktopControls').removeEventListener('mouseout', desktopControlsMouseOut, false);
}
function videoListeners() {
	videojs(videoId).on('ended', function() {
		if (currentNavMode === 'shows') {
			mPlayer.playlist.currentItem(targetEpisodes.indexOf(currentShow.firstEpisode));
		} else if (currentNavMode === 'episodes') {
			var i = ((targetEpisodes.indexOf(currentEpisode) + 1) + targetEpisodes.length) % targetEpisodes.length;
			currentEpisode = targetEpisodes[i];
			nextEpisode = targetEpisodes[((i + 1) + targetEpisodes.length) % targetEpisodes.length];
			prevEpisode = targetEpisodes[((i - 1) + targetEpisodes.length) % targetEpisodes.length];

			if (currentEpisode.show !== currentShow.title) {
				for (var j = 0; j < targetShows.length; j++) {
					if (currentEpisode.show === targetShows[j].title) {
						currentShow = targetShows[j];
						nextShow = targetShows[((i + 1) + targetShows.length) % targetShows.length];
						prevShow = targetShows[((i - 1) + targetShows.length) % targetShows.length];
					}
				}
			}
			refreshFaces();
		} else if (currentNavMode === 'playlists') {
			mPlayer.playlist.currentItem(targetEpisodes.indexOf(currentPlaylist.firstVideo));
		} else if (currentNavMode === 'videos') {
			var i = ((targetVideos.indexOf(currentVideo) + 1) + targetVideos.length) % targetVideos.length;
			currentVideo = targetVideos[i];
			nextVideo = targetVideos[((i + 1) + targetVideos.length) % targetVideos.length];
			prevVideo = targetVideos[((i - 1) + targetVideos.length) % targetVideos.length];
			if (currentVideo.playlist !== currentPlaylist.title) {
				for (var j = 0; j < targetPlaylists.length; j++) {
					if (currentVideo.playlist === targetPlaylists[j].title) {
						currentPlaylist = targetPlaylists[j];
						nextPlaylist = targetPlaylists[((i + 1) + targetPlaylists.length) % targetPlaylists.length];
						prevPlaylist = targetPlaylists[((i - 1) + targetPlaylists.length) % targetPlaylists.length];
					}
				}
			}
			refreshFaces();
		}
	})

	videojs(videoId).on('play', function() {
		// previewLoop();
		// updateBackgroundVideo();
		TweenMax.to(videoPlayer, 0.5, {alpha:1})
		playControl.classList.remove('paused');
		playControl.setAttribute('aria-label', 'pause');
		TweenMax.delayedCall(2, hideInfo);
	})
	videojs(videoId).on('pause', function() {
		playControl.classList.add('paused');
		playControl.setAttribute('aria-label', 'play');
		if (!transitionReady) {
			showInfo();
		}
	})
	document.querySelector('#videoPlayer').addEventListener('mouseover', function() {
		TweenMax.killTweensOf('.vjs-control-bar');
		TweenMax.to('.vjs-control-bar', 0.5, {y:0});
		if (document.querySelector('#navToggler')) {
			TweenMax.killTweensOf('.navbar-static-top');
			TweenMax.to('.navbar-static-top', 0.5, {y:0});
		}
	}, false);
	document.querySelector('#videoPlayer').addEventListener('mouseleave', function() {
		TweenMax.killTweensOf('.vjs-control-bar');
		TweenMax.to('.vjs-control-bar', 0.5, {y:100});
		if (document.querySelector('#navToggler') && currentNavMode === 'episodes' || currentNavMode === 'videos') {
			TweenMax.killTweensOf('.navbar-static-top');
			TweenMax.to('.navbar-static-top', 0.5, {y:-100});
		}
	}, false);
	if (document.querySelector('#navToggler')) {
		document.querySelector('#navToggler').addEventListener('mouseover', function() {
			TweenMax.killTweensOf('.navbar-static-top');
			TweenMax.to('.navbar-static-top', 0.5, {y:0});
		}, false);
	}
	document.querySelector('.vjs-control-bar > .vjs-subs-caps-button').insertAdjacentHTML('beforeend', '<div class="vjs-cc-button"></div>')
	ccButton = document.querySelector('.vjs-cc-button');
	ccButton.addEventListener('click', toggleTextTracks, false)
}
function zoomIn() {
	// TweenMax.to([videoPlayer, xFaces, yFaces], 0.3, {z:0})

	// TweenMax.set([videoPlayer, xFaces, yFaces], {css:{boxShadow:'0px 0px 0px #fde147'}});
}
function zoomOut() {
	// TweenMax.to([videoPlayer, xFaces, yFaces], 0.3, {z:-50})

	// TweenMax.set([videoPlayer, xFaces, yFaces], {css:{boxShadow:'0px 0px 0px #fde147'}});
	// if (nullObject._gsTransform.x > 0) {
		// TweenMax.set(videoPlayer, {css:{boxShadow:'-4px 0px 0px #fde147'}});
		// TweenMax.set(leftFace, {css:{boxShadow:'4px 0px 0px #fde147'}});
	// } else if (nullObject._gsTransform.x < 0) {
		// TweenMax.set(videoPlayer, {css:{boxShadow:'4px 0px 0px #fde147'}});
		// TweenMax.set(rightFace, {css:{boxShadow:'-4px 0px 0px #fde147'}});
	// } else if (nullObject._gsTransform.y > 0) {
		// TweenMax.set(videoPlayer, {css:{boxShadow:'0px -4px 0px #fde147'}});
		// TweenMax.set(topFace, {css:{boxShadow:'0px 4px 0px #fde147'}});
	// } else if (nullObject._gsTransform.y < 0) {
		// TweenMax.set(videoPlayer, {css:{boxShadow:'0px 4px 0px #fde147'}});
		// TweenMax.set(bottomFace, {css:{boxShadow:'0px -4px 0px #fde147'}});
	// }
}

function hideInfo() {
	if (TweenMax.isTweening([parentTitle, mainTitle, subTitle])) {return false;}
	TweenMax.killTweensOf([parentTitle, mainTitle, subTitle]);
	TweenMax.killTweensOf(showInfo);
	if ( (currentNavMode === 'shows') || (currentNavMode === 'playlists') || mPlayer.paused() ) {
		TweenMax.killTweensOf(hideInfo);
	}
	// if (frontFace._gsTransform.rotationX === 0 && frontFace._gsTransform.rotationY === 0) { return false };
	TweenMax.to(['.vjs-control-bar','.site-footer'], 0.5, {y:100, rotation:0.001, force3D:true})
	if (document.querySelector('#navToggler') && currentNavMode === 'episodes' || currentNavMode === 'videos') {
		TweenMax.to('.navbar-static-top', 0.5, {y:-100})
	}
	if (!isMobile) {
		TweenMax.to('.desktopControls', 0.5, {scale:0.75, ease:Elastic.easeOut.config(1, 0.5), rotation:0.001, force3D:true})
	}
	TweenMax.to([parentTitle, mainTitle, subTitle], 0.3, {x:-screenWidth*1.5, y:0, force3D:true})
}
function showInfo() {
	if ( currentNavMode === 'shows' || currentNavMode === 'playlists' || (mPlayer.paused() && !transitionReady) ) {
		TweenMax.killTweensOf([parentTitle, mainTitle, subTitle]);
		TweenMax.killTweensOf(showInfo);
		TweenMax.killTweensOf(hideInfo);
	}

	TweenMax.to(['.vjs-control-bar','.site-footer'], 0.5, {y:0});
	if (document.querySelector('#navToggler')) {
		TweenMax.to('.navbar-static-top', 0.5, {y:0})
	}
	if (currentNavMode === 'shows') {
		mainTitle.className = 'showTitleLrg';
		parentTitle.innerHTML = '';
		mainTitle.innerHTML = currentShow.title_formatted;
		if (currentShow.description.indexOf(' ') < 2) {
			subTitle.innerHTML = `<span class="showUnderline">${currentShow.description.slice(0, currentShow.description.indexOf('.')+1)}</span> ${currentShow.description.slice(currentShow.description.indexOf('.')+1)}`;
		} else {
			subTitle.innerHTML = `<span class="showUnderline">${currentShow.description.slice(0, currentShow.description.indexOf(' '))}</span> ${currentShow.description.slice(currentShow.description.indexOf(' '))}`;
		}
	} else if (currentNavMode === 'episodes') {
		parentTitle.className = 'showTitleSml';
		mainTitle.className = 'episodeTitle';
		parentTitle.innerHTML = currentEpisode.show;
		mainTitle.innerHTML = currentEpisode.title_formatted;
		if (currentEpisode.description.indexOf(' ') < 2) {
			subTitle.innerHTML = `<span class="episodeUnderline">${currentEpisode.description.slice(0, currentEpisode.description.indexOf('.')+1)}</span> ${currentEpisode.description.slice(currentEpisode.description.indexOf('.')+1)}`;
		} else {
			subTitle.innerHTML = `<span class="episodeUnderline">${currentEpisode.description.slice(0, currentEpisode.description.indexOf(' '))}</span> ${currentEpisode.description.slice(currentEpisode.description.indexOf(' '))}`;
		}
	} else if (currentNavMode === 'playlists') {
		mainTitle.className = 'playlistTitleLrg';
		parentTitle.innerHTML = '';
		mainTitle.innerHTML = currentPlaylist.title_formatted;
		if (currentPlaylist.description.indexOf(' ') < 2) {
			subTitle.innerHTML = `<span class="playlistUnderline">${currentPlaylist.description.slice(0, currentPlaylist.description.indexOf('.')+1)}</span> ${currentPlaylist.description.slice(currentPlaylist.description.indexOf('.')+1)}`;
		} else {
			subTitle.innerHTML = `<span class="playlistUnderline">${currentPlaylist.description.slice(0, currentPlaylist.description.indexOf(' '))}</span> ${currentPlaylist.description.slice(currentPlaylist.description.indexOf(' '))}`;
		}
	} else if (currentNavMode === 'videos') {
		parentTitle.className = 'playlistTitleSml';
		mainTitle.className = 'episodeTitle';
		parentTitle.innerHTML = currentVideo.playlist;
		mainTitle.innerHTML = currentVideo.title;
		if (currentVideo.description.indexOf(' ') < 2) {
			subTitle.innerHTML = `<span class="episodeUnderline">${currentVideo.description.slice(0, currentVideo.description.indexOf('.')+1)}</span> ${currentVideo.description.slice(currentVideo.description.indexOf('.')+1)}`;
		} else {
			subTitle.innerHTML = `<span class="episodeUnderline">${currentVideo.description.slice(0, currentVideo.description.indexOf(' '))}</span> ${currentVideo.description.slice(currentVideo.description.indexOf(' '))}`;
		}
	}
	// if (parentTitle && parentTitle._gsTransform && parentTitle._gsTransform.x === 0) { return };
	TweenMax.set([parentTitle, mainTitle, subTitle], {x:0, y:0})
	TweenMax.fromTo([parentTitle, mainTitle, subTitle], 0.3, {x:faceWidth, y:0, alpha:0}, {x:0, y:0, alpha:1});
}

function toggleTextTracks() {
	var textTracks = mPlayer.textTracks();
	for (let i = 0; i < textTracks.length; i++) {
		var textTrack = textTracks[i];
		if ((textTrack.kind === 'captions' || textTrack.kind === 'subtitles') && textTrack.language === 'en-US') {
			if (textTrack.mode === 'disabled') {
				textTrack.mode = 'showing';
				TweenMax.set('.vjs-cc-button', {alpha:1})
			} else if (textTrack.mode === 'showing') {
				textTrack.mode = 'disabled';
				TweenMax.set('.vjs-cc-button', {alpha:0.6})
			}
		}
	}
}
function keyboardControls(event) {
	if (event.key === 'ArrowUp') {
		hideInfo('down');
		rotateCube('up');
		document.querySelector('.navButton[data-direction="up"]').classList.add('hover');
		TweenMax.delayedCall(1, function() { document.querySelector('.navButton[data-direction="up"]').classList.remove('hover') });
	} else if (event.key === 'ArrowDown') {
		hideInfo('up');
		rotateCube('down');
		document.querySelector('.navButton[data-direction="down"]').classList.add('hover');
		TweenMax.delayedCall(1, function() { document.querySelector('.navButton[data-direction="down"]').classList.remove('hover') });
	} else if (event.key === 'ArrowRight') {
		hideInfo('right');
		rotateCube('right');
		document.querySelector('.navButton[data-direction="right"]').classList.add('hover');
		TweenMax.delayedCall(1, function() { document.querySelector('.navButton[data-direction="right"]').classList.remove('hover') });
	} else if (event.key === 'ArrowLeft') {
		hideInfo('left');
		rotateCube('left');
		document.querySelector('.navButton[data-direction="left"]').classList.add('hover');
		TweenMax.delayedCall(1, function() { document.querySelector('.navButton[data-direction="left"]').classList.remove('hover') });
	} else if (event.keyCode === 32) {
		onClick();
	} else if (event.key === 'Tab') {
		TweenMax.killTweensOf('.vjs-control-bar');
		TweenMax.to('.vjs-control-bar', 0.5, {y:0});
	}
	document.removeEventListener('keyup', keyboardControls, false);
}
function desktopControlsMouseOver(e) {
	e.target.classList.add('hover');
	TweenMax.to('.desktopControls', 0.5, {scale:1, ease:Elastic.easeOut.config(1, 0.5)})
}
function desktopControlsMouseOut(e) {
	e.target.classList.remove('hover');
	TweenMax.to('.desktopControls', 0.5, {scale:0.65, ease:Elastic.easeOut.config(1, 0.5)})
}
function previewLoop() {
	TweenMax.killDelayedCallsTo(showLoop);
	TweenMax.killDelayedCallsTo(playlistLoop);
	if (currentNavMode === 'shows') {
		TweenMax.delayedCall(currentEpisode.loop_end - currentEpisode.loop_start, showLoop)
	} else if (currentNavMode === 'playlists') {
		TweenMax.delayedCall(currentVideo.loop_end - currentVideo.loop_start, playlistLoop)
	}
}
function showLoop() {
	mPlayer.currentTime(currentEpisode.loop_start);
	previewLoop();
}
function playlistLoop() {
	mPlayer.currentTime(currentVideo.loop_start);
	previewLoop();
}
function updateBackgroundVideo() {
	// ANIMATED VERSION
	// if( isMobile || (currentNavMode === 'episodes') || (currentNavMode === 'videos')) {
	// 	return false;
	// }
	// if ( mPlayer.paused() || mPlayer.ended() ) { return false };
	// bgContext.drawImage(videoPlayer, 0, 0, bg.width, bg.height);
	// bgContext.drawImage(videoPlayer, 0, 0, bg.width, bg.height);
	// TweenMax.delayedCall(0.06, updateBackgroundVideo);

	// MOBILE VERSION, EDGE:
	if (isMobile || isEdge) {
		bgContext.fillStyle = "#404040";
		bgContext.fillRect(0, 0, bg.width, bg.height);
		return true;
	}

	// DEGRADED VERSION:
	if (currentNavMode === 'shows' || currentNavMode === 'episodes') {
		bgContext.drawImage(currentEpisode.cover, 0, 0, bg.width, bg.height);
	}	else if (currentNavMode === 'playlists' || currentNavMode === 'videos') {
		bgContext.drawImage(currentVideo.cover, 0, 0, bg.width, bg.height);
	}
}
function getScreenDimensions() {
	screenWidth = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
	faceHeight = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);
	faceWidth = faceHeight * videoAspectRatio;
}
function getZOriginX() {
	var faceZOrigin = (faceHeight/2) / Math.tan(DegreesToRadians((rotationStep/2)));
	return faceZOrigin;
}
function getZOriginY() {
	var faceZOrigin = (faceWidth/2) / Math.tan(DegreesToRadians((rotationStep/2)));
	return faceZOrigin;
}
function DegreesToRadians(valDeg) {
	return ((2*Math.PI)/fullRotation*valDeg)
}
function randStr() {
	var text = '';
	var chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
	for (var i = 0; i < 8; i++) {
		text += chars[Math.floor(Math.random() * chars.length)]
	}
	return text.toLowerCase();
}
function doubleDigit(n){
	return n > 9 ? ""+n : "0"+n;
}
function underscoreEncode(name) {
	var text = '';
	var chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
	for (var i = 0; i < name.length; i++) {
		if (chars.indexOf(name[i]) < 0) {
			text += '_';
		} else {
			text += name[i];
		}
	}
	return text.toLowerCase();
}

function initPreloader() {
	hideInfo();
	TweenMax.set(['.desktopControls', '.site-footer'], {alpha:0})
	TweenMax.to('#preloader-logo', 0.5, {scale:0.85, repeat:100, yoyo:true, transformOrigin:'center center'})
}

function hidePreloader() {
	TweenMax.killTweensOf('#preloader-logo')
	TweenMax.to('#preloader-logo', 0.15, {scale:1})
	var d = 1;
	TweenMax.to('#preloader-logo', 0.3, {alpha:0, ease: Power4.easeIn, transformOrigin:'center center', delay:1.7})

	TweenMax.to('#preloader', 1.5, {width:0, height:0, transformOrigin:'center center', x:0, y:`-${faceHeight/2 -42}`, delay:d, ease: Power3.easeOut, onComplete:function() {
		TweenMax.set('#preloader-logo', {alpha:0})
		TweenMax.set(['.site-footer'], {alpha:1})
		TweenMax.to(videoInfo, 0.5, {alpha:1})
		showInfo();
	}})

	if (document.getElementsByClassName('navbar-wrapper')[0]) {document.getElementsByClassName('navbar-wrapper')[0].style.visibility='visible'};
	TweenMax.fromTo('.navbar-wrapper', 0.2, {alpha:0, scale:0}, {alpha:1, scale:1, delay:2})
}

// function introSequence() {
// 	hideInfo();
// 	myCubeDraggable[0].disable();
// 	currentNavMode = 'intro';
//
// 	// mPlayer.catalog.load(introPlaylist);
// 	mPlayer.catalog.load(introPlaylist);
// 	if (faceHeight > screenWidth) { // load mobile video
// 		mPlayer.playlist.currentItem(1);
// 	} else {
// 		mPlayer.playlist.currentItem(0);
// 		TweenMax.set(videoPlayer, {width:faceHeight*16/9, height:faceHeight, transformOrigin:'center center'})
// 		mPlayer.width(faceHeight*16/9)
// 		mPlayer.height(faceHeight)
//
// 		TweenMax.delayedCall(16.5, function() {
// 			var clipSide1 = [0];
// 			var clipSide2 = [((faceHeight*16/9)-faceWidth)/2];
// 			clipSide2.onUpdate = function() {
// 			  TweenMax.set(videoPlayer, {webkitClipPath:`inset(0px ${clipSide1[0]}px 0px ${clipSide1[0]}px)`});
// 			};
// 			TweenLite.to(clipSide1, 5, clipSide2);
// 		})
// 	}
//
// 	// rotate
// 	if (rightFace) { container.removeChild(rightFace) }
// 		rightFace = document.createElement('div');
// 	rightFace.className = 'face';
// 	TweenMax.set(rightFace, {position:'absolute', width:faceWidth, height:faceHeight, zIndex:1, transformPerspective:1000, backfaceVisibility:'hidden', transformOrigin:'50% 50% ' + -faceZOriginY + 'px', boxSizing:'border-box', rotationY: rotationStep, force3D:true});
// 	rightFace.initRotationX = 0;
// 	rightFace.initRotationY = rotationStep;
// 	yFaces.push(rightFace);
// 	container.appendChild(rightFace);
// 	rightFace.appendChild(currentShow.cover);
// 	TweenMax.delayedCall(22, function() {
// 		TweenMax.set(videoPlayer, {css:{boxShadow:'4px 0px 0px #fde147'}});
// 		TweenMax.set(rightFace, {css:{boxShadow:'-4px 0px 0px #fde147'}});
// 		swipeDirection = 'left';
// 		TweenMax.to([videoPlayer, frontFace, rightFace], 0.75, {
// 			transformOrigin:'50% 50% ' + -faceZOriginY + 'px',
// 			rotationY:`-=90`,
// 			ease:Power4.easeOut,
// 			force3D:true,
// 			onComplete: function() {
// 				TweenMax.set(videoPlayer, {width:faceWidth, height:faceHeight, webkitClipPath:'inset(0px 0px 0px 0px)'})
// 				mPlayer.width(faceWidth);
// 				mPlayer.height(faceHeight);
// 				mPlayer.pause()
// 				mPlayer.catalog.load(episodesPlaylist);
// 				mPlayer.catalog.load(episodesPlaylist);
// 				currentNavMode = 'shows';
// 				TweenMax.to('.desktopControls', 1, {alpha:1})
// 				myCubeDraggable[0].enable();
// 				refreshFaces();
// 				frontFace.style.zIndex = 0;
// 				frontFace.style.boxShadow = '';
// 			}
// 		})
// 	})
//
//
// }
