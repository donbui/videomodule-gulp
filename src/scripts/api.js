var client_id = '521d7613-44e5-4dbe-9c73-c9db66721206';
var client_secret = 'U_0UDqMCCFR6HiiOyXq2oFYyStNEv4B3fZfRiyf1kNm8UTlql22eA8vHuIeYNakBKpTSMV4_xcGfZSN9dK5eRQ';

var baseURL = 'https://cms.api.brightcove.com/v1';
var account_id = '1894469414001';
var player_id = 'H1maWx6EX';

var introPlaylistId, showsPlaylistId, playlistsPlaylistId, episodesPlaylistId, videosPlaylistId, introPlaylist, episodesPlaylist, videosPlaylist;
var req = [];

getAuth()
.then((access_token) => {
	initPreloader();
	setAuth(access_token)
	getInfo().then(() => {

		Promise.all(req).then(() => {
			console.log('Content retrieved')
			for (var i = 0; i < targetShows.length; i++) {
				for (var j = 0; j < targetEpisodes.length; j++) {
					if (targetShows[i].title === targetEpisodes[j].show) {
						targetShows[i].firstEpisode = targetEpisodes[j];
						break;
					}
				}
			}
			for (var i = 0; i < targetPlaylists.length; i++) {
				for (var j = 0; j < targetVideos.length; j++) {
					if (targetPlaylists[i].title === targetVideos[j].playlist) {
						targetPlaylists[i].firstVideo = targetVideos[j];
						break;
					}
				}
			}

			initUI();
			createFrontFace();
			createAdjFaces();
			setCubeDraggable();

			// TODO: fix this async issue
			videojs(videoId).catalog.getPlaylist(videosPlaylistId, function(error, _videosPlaylist) {
				if (error) {console.log(error); return;}
				videosPlaylist = _videosPlaylist;

				videojs(videoId).catalog.getPlaylist(episodesPlaylistId, function(error, _episodesPlaylist) {
					if (error) {console.log(error); return;}
					episodesPlaylist = _episodesPlaylist;

					videojs(videoId).catalog.getPlaylist(introPlaylistId, function(error, _introPlaylist) {
						if (error) {console.log(error); return;}
						introPlaylist = _introPlaylist;

						updateFaces();
					});
				});
			});

		})

	}).catch((error) => {
		console.log(error)
	})
})


function getAuth() {
	return new Promise((resolve, reject) => {
		axios.post('https://oauth.brightcove.com/v4/access_token?grant_type=client_credentials',
			`client_id=${client_id}&client_secret=${client_secret}`,
		).then((response) => {
			resolve(response.data.access_token);
		}).catch((error) => {
			reject(error);
		})
		// axios.get(document.location.origin + '/wp-content/themes/asa/public/brightcove/api.php?action=get_token',
		// ).then((response) => {
		// 	resolve(response.data.data);
		// }).catch((error) => {
		// 	reject(error);
		// })
	})
}

function setAuth(access_token) {
	axios.defaults.headers.common['Authorization'] = `Bearer ${access_token}`;
	return;
}

function getInfo() {
	return new Promise((resolve, reject) => {
		axios.get(`${baseURL}/accounts/${account_id}/playlists`).then((response) => {

			for (var i = 0; i < response.data.length; i++) {
				if (response.data[i].name === '_shows') {
					showsPlaylistId = response.data[i].id;
				} else if (response.data[i].name === '_episodes') {
					episodesPlaylistId = response.data[i].id;
				} else if (response.data[i].name === '_playlists') {
					playlistsPlaylistId = response.data[i].id;
				} else if (response.data[i].name === '_playlistVideos') {
					videosPlaylistId = response.data[i].id;
				} else if (response.data[i].name === '_intro') {
					introPlaylistId = response.data[i].id;
				}
			}
			// TODO: check to see that all 3 IDs were retrieved succesfully

			req.push(axios.get(`${baseURL}/accounts/${account_id}/playlists/${showsPlaylistId}/videos`).then((shows) => {
				var activeCounter = 0;
				for (var j = 0; j < shows.data.length; j++) {
					if (shows.data[j].state === 'ACTIVE') {
						targetShows.push({
							title: shows.data[j].name,
							title_formatted: shows.data[j].custom_fields.htmlname ? shows.data[j].custom_fields.htmlname : shows.data[j].name,
							description: shows.data[j].description,
							cover: new Image()
						})
						targetShows[activeCounter].cover.src = shows.data[j].images.poster.sources[0].src;
						targetShows[activeCounter].cover.className = 'coverImg';
					}
					activeCounter++;
				}
				currentShow = targetShows[0];
				nextShow = targetShows[1];
				prevShow = targetShows[targetShows.length - 1];
			}))

			req.push(axios.get(`${baseURL}/accounts/${account_id}/playlists/${episodesPlaylistId}/videos`).then((episodes) => {
				var activeCounter = 0;
				for (var k = 0; k < episodes.data.length; k++) {
					if (episodes.data[k].state === 'ACTIVE') {
						targetEpisodes.push({
							title: episodes.data[k].name,
							title_formatted: episodes.data[k].custom_fields.htmlname ? episodes.data[k].custom_fields.htmlname : episodes.data[k].name,
							description: episodes.data[k].description,
							cover: new Image(),
							show: episodes.data[k].custom_fields.show,
							playlist: episodes.data[k].custom_fields.playlist,
							id: episodes.data[k].id,
							loop_start: episodes.data[k].custom_fields.loopstart ? Number(episodes.data[k].custom_fields.loopstart) : 0.5,
							loop_end: episodes.data[k].custom_fields.loopend ? Number(episodes.data[k].custom_fields.loopend) : 3
						})
						targetEpisodes[activeCounter].cover.src = episodes.data[k].images.poster.sources[0].src;
						targetEpisodes[activeCounter].cover.className = 'coverImg';
						activeCounter++;
					}
				}
				currentEpisode = targetEpisodes[0];
				nextEpisode = targetEpisodes[1];
				prevEpisode = targetEpisodes[targetEpisodes.length - 1];
			}))

			req.push(axios.get(`${baseURL}/accounts/${account_id}/playlists/${playlistsPlaylistId}/videos`).then((playlists) => {
				var activeCounter = 0;
				for (var l = 0; l < playlists.data.length; l++) {
					if (playlists.data[l].state === 'ACTIVE') {
						targetPlaylists.push({
							title: playlists.data[l].name,
							title_formatted: playlists.data[l].custom_fields.htmlname ? playlists.data[l].custom_fields.htmlname : playlists.data[l].name,
							description: playlists.data[l].description,
							cover: new Image()
						})
						targetPlaylists[activeCounter].cover.src = playlists.data[l].images.poster.sources[0].src;
						targetPlaylists[activeCounter].cover.className = 'coverImg';
						activeCounter++;
					}
				}
				currentPlaylist = targetPlaylists[0];
				nextPlaylist = targetPlaylists[(1 + targetPlaylists.length) % targetPlaylists.length];
				prevPlaylist = targetPlaylists[(-1 + targetPlaylists.length) % targetPlaylists.length];
			}))

			req.push(axios.get(`${baseURL}/accounts/${account_id}/playlists/${videosPlaylistId}/videos`).then((videos) => {
				var activeCounter = 0, counter = 1;
				for (var m = 0; m < videos.data.length; m++) {
					if (videos.data[m].state === 'ACTIVE') {
						if ((m > 0) && (videos.data[m].custom_fields.playlist !== videos.data[m-1].custom_fields.playlist)) {
							counter = 1;
						} else if (m > 0) {
							counter++;
						};
						targetVideos.push({
							title: `E.${doubleDigit(counter)}`,
							// title_formatted: videos.data[m].custom_fields.htmlname ? videos.data[m].custom_fields.htmlname : videos.data[m].name,
							description: videos.data[m].description,
							cover: new Image(),
							playlist: videos.data[m].custom_fields.playlist,
							show: videos.data[m].custom_fields.show,
							id: videos.data[m].id,
							loop_start: videos.data[m].custom_fields.loopstart ? Number(videos.data[m].custom_fields.loopstart) : 0.1,
							loop_end: videos.data[m].custom_fields.loopend ? Number(videos.data[m].custom_fields.loopend) : 3
						})
						targetVideos[activeCounter].cover.src = videos.data[m].images.poster.sources[0].src;
						targetVideos[activeCounter].cover.className = 'coverImg';
						activeCounter++;
					}
				}
				currentVideo = targetVideos[0];
				nextVideo = targetVideos[1];
				prevVideo = targetVideos[targetVideos.length - 1];
			}))

			resolve();
		}).catch((error) => {
			reject(error);
		})
	})
}
