var gulp = require('gulp'),
	error = require('plugin-error');
	sass = require('gulp-sass'),
	babel = require('gulp-babel'),
	uglify = require('gulp-uglify'),
	concat = require('gulp-concat'),
	merge = require('gulp-merge'),
	connect = require('gulp-connect');

var BABEL_POLYFILL = './node_modules/babel-polyfill/browser.js';
var SEARCH_PARAMS = 'src/scripts/url-search-params-polyfill.js';
var AXIOS = 'src/scripts/gsap/axios.min.js';
var DRAGGABLE = 'src/scripts/gsap/Draggable.min.js';
var BCAPI = 'src/scripts/gsap/bc-api.min.js';
var TWEEN = 'src/scripts/gsap/TweenMax.min.js';
var THROW = 'src/scripts/gsap/ThrowPropsPlugin.min.js';

var jsSources = ['src/scripts/cube.js', 'src/scripts/api.js'],
		sassSources = ['src/styles/*.scss'],
    outputDir = 'public';

gulp.task('copy', function() {
  gulp.src('src/index.html')
  .pipe(gulp.dest(outputDir))
	gulp.src('src/scripts/gsap/*.js')
  .pipe(gulp.dest(outputDir))
});

gulp.task('sass', function() {
  gulp.src(sassSources)
  .pipe(sass({style: 'expanded'}))
    // .on('error', gutil.log)
  .pipe(gulp.dest(outputDir))
	.pipe(connect.reload())
});

gulp.task('js', function() {
	merge(
		gulp.src(AXIOS),
		gulp.src(TWEEN),
		gulp.src(DRAGGABLE),
		gulp.src(THROW),
		gulp.src(BCAPI),
		gulp.src(SEARCH_PARAMS),
    gulp.src(BABEL_POLYFILL),
  	gulp.src(jsSources)
			.pipe(babel({presets: ['env']}))
  		.pipe(uglify())
	)
  	.pipe(concat('script.js'))
  	.pipe(gulp.dest(outputDir))
		.pipe(connect.reload())
});

gulp.task('watch', function() {
	gulp.watch(jsSources, ['js']);
	gulp.watch(sassSources, ['sass']);
});

gulp.task('connect', function() {
  connect.server({
    root: 'public',
    livereload: true
  })
});

gulp.task('default', ['copy', 'sass', 'js', 'watch', 'connect']);
gulp.task('compile', ['copy', 'sass', 'js']);
