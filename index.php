<div id="app" role="application">
	<canvas id="backgroundVideo"></canvas>
	<div class="container">
		<!-- <div class="muted"></div> -->
		<!-- <div class="scrollable"></div> -->
		<div id="videoInfo">
			<div id="parentTitle"></div>
			<div id="mainTitle"></div>
			<div id="subTitle" class="description"></div>
		</div>
	</div>
	<div class="desktopControls">
		<div class="navUp navButton" data-direction="up" tabindex="0"></div>
		<div class="navLeft navButton" data-direction="left" tabindex="0"></div>
		<div class="navRight navButton" data-direction="right" tabindex="0"></div>
		<div class="navDown navButton" data-direction="down" tabindex="0"></div>
	</div>
</div>
